# OSI and TCP/IP Theory

.footer: created by Alex M. Schapelle, VaioLabs.IO

---
# Network types an attributes

- Circuit switching vs packet switching
- Message transmission methods
    - Unicast
    - Broadcast
    - Anycast
- Main ways of communication:
    - Simplex : one way communication system
    - Half-duplex : two way communication system with que (wacky-talky, eth-hub)
    - Full-duplex : phone call
- Types of networks:
    - Inter-nets: many existing networks with in one big(global) network
    - Intra-nets: internal networks with in one existing network
    - Extra-nets: dedicated network for service or vendor that has access for that vendors data.
---
# Network types an attributes (cont.)

- Additional view on networks:
    - PAN: Personal Area Network (Bluetooth)
    - LAN: Local Area Network (home network)
    - WLAN: Wireless Local Area Network (home network with wireless)
    - CAN: Campus Area Network (stand alone network of specific network)
    - MAN: Metropolitan Area Network (Gov networks, like police and municipal)
    - WAN: Wide Area Network

---
# Network hardware

- PC/Node/Server/Endpoint
- Router
- Switch
- Access Point
- Network Interface Card (NIC)
- Cabling: 
    - Ethernet
    - Coax
    - Fiber
---
# Math Behind Network

Everything in computers is based on math in some manner.

What do we need to know ?

Bases:
  - Decimal
  - Binary
  - Hexadecimal

--- 
# Decimal Base

<img src="../misc/.img/base-dec.png" alt="drawing" style="float:center;width:400px;">


---
# Binary Base
<img src="../misc/.img/base-bin.png" alt="drawing" style="float:center;width:400px;">


---
# Hexadecimal Base

<img src="../misc/.img/base-hex.png" alt="drawing" style="float:center;width:400px;">


---
# Whats the point of it ?

- None. But it can be helpful in cases when you need to calculate amount of device IP address over the network 
- One can use `ipcalc` utility to do the calculations for you.

---
# Exercise

- Convert those next values from binary to decimal:
  - 00101010  <!--42-->
  - 100001111 <!--271-->
  - 11000101  <!--197-->
  
- Convert those next values from hexadecimal to decimal:
  - 6F  <!--111-->
  - 1BB <!--443-->
  - 35  <!--53 -->

---
# Open Systems Inter-communication Model (OSI)

The Open Systems Interconnection model (OSI model) is a conceptual model that characterizes and standardizes the communication functions of a telecommunication or computing system without regard to its underlying internal structure and technology. Its goal is the interoperability of diverse communication systems with standard communication protocols. 
The OSI model was developed starting in the late 1970s to support the emergence of the diverse computer networking methods that were competing for application in the large national networking efforts in the world.

What does it all mean? Someone, some time ago, suggested how the computational systems should communicate with each other in theory.

---

# OSI Model Layer Architecture

The recommended OSI model describes 7 layers, labelled 1 to 7. Layer 1 is the lowest layer in this model. 

| Layer              | Protocol data unit (PDU) | Function | 
| ---                |               ---        |   ---    |
| 7 : Application    |  Data    | High-level APIs, including resource sharing, remote file access |
| 6 : Presentation   |  Data    | Translation of data between a networking service and an application; including character encoding, data compression and encryption/decryption  |
| 5 : Session        |  Data    | Managing communication sessions, i.e., continuous exchange of information in the form of multiple back-and-forth transmissions between two nodes  |
| 4 : Transport      |  Segment | Reliable transmission of data segments between points on a network, including segmentation, acknowledgement and multiplexing |
| 3 : Network        |  Packet | Structuring and managing a multi-node network, including addressing, routing and traffic control | 
| 2 : Data-Link      |  Frame  | Reliable transmission of data frames between two nodes connected by a physical layer |
| 1 : Physical       |  Bit | Transmission and reception of raw bit streams over a physical medium |

---
# Layer 7: Application Layer

The application layer is the OSI layer closest to the end user, which means both the OSI application layer and the user interact directly with the software application.

---
# Layer 6: Presentation Layer

The presentation layer establishes context between application-layer entities, in which the application-layer entities may use different syntax and semantics if the presentation service provides a mapping between them. If a mapping is available, presentation protocol data units are encapsulated into session protocol data units and passed down the protocol stack. 

---
# Layer 5: Session Layer

The session layer controls the dialogues (connections) between computers. It establishes, manages and terminates the connections between the local and remote application

---
# Layer 4: Transport Layer

The transport layer provides the functional and procedural means of transferring variable-length data sequences from a source to a destination host, while maintaining the quality of service functions.

---
# Layer 3: Network Layer

The network layer provides the functional and procedural means of transferring packets from one node to another connected in "different networks".

---
# Layer 2: Data Link Layer

The data link layer provides node-to-node data transfer—a link between two directly connected nodes. It detects and possibly corrects errors that may occur in the physical layer. It defines the protocol to establish and terminate a connection between two physically connected devices. It also defines the protocol for flow control between them. 

---
# Layer 1: Physical Layer

The physical layer is responsible for the transmission and reception of unstructured raw data between a device and a physical transmission medium. It converts the digital bits into electrical, radio, or optical signals. Layer specifications define characteristics such as voltage levels, the timing of voltage changes, physical data rates, maximum transmission distances, modulation scheme, channel access method and physical connectors

---

# Additional Layers

As time progresses, theories, protocols and such gets update, but we are not going to cover it, due to irrelevance.

---
# Comparing OSI 7 Layer to TCP/IP

<img src="../misc/.img/tcp.ip.osi7.png" alt="drawing" style="float:center;width:500px;">

---

# TCP/IP Layers
## Link Layer
- TCP/IP supports many different link layers including:
  - Ethernet LANs
  - DSL, Cable(DOCSIS) and FiberDDI
  - Wired voice Networks (VoIP)
  - Wireless LANs
  - Cellular and Satellite

---

# TCP/IP Layers (cont.)

## Internet Layer

<img src="../misc/.img/layer3_ip.png" alt="drawing" style="float:center;width:500px;">

---
# TCP/IP Layers (cont.)

## Transport Layer (Host-To-Host)

- Enables end-to-end communication over inter-networks.
- Logical connections(ports) are made between hosts in a reliable (connection-oriented) or unreliable manner (connection-less).
- The TCP/IP transport layer includes specific functionality of the OSI layer 5 (Session) as well.

---
# TCP/IP Layers (cont.)

## Application Layer

- All applications on our Nodes/PCs use TCP/IP
- Desktop Applications use combination of protocols based on TCP/IP


---

# TCP/IP suite:

## List of suite protocols

The short list of TCP/IP based protocols to aid us with understanding further material:
- SSH: Secured Shell
- HTTP/S: Hypertext Trasnfer Protocol
- FTP: File Transfer Protocol
- NTP: Network Time Protocol
- DNS: Dynamic Name Service
- DHCP: Dynamic Host Configuration Protocol
- NFS: Network File Server
- SAMBA: Server Message Block Protocol