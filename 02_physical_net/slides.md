# Network, Physical Devices, Linux and Other Ducks

.footer: created by Alex M. Schapelle, VaioLabs.IO

---
# Physical Devices

- Devices:
  - Endpoint Devices
    - Servers
    - Desktops
    - Laptops
    - NICs: Network Interfaces Cards

---

# Physical Devices (cont.)

  - Wireless Devices
    - Access Points
    - Pads
    - SmartPhones

---

# Physical Devices (cont.)

- Devices:
  - Local Area Network Devices
    - Switches
    - L3 Switches
    - Hubs
    - Wireless Routers
    - Bridges
    - Modems
    - Firewalls
---

# Physical Devices (cont.)

- Devices:
  - Wide Area Network Devices  
    - Routers
    - L3 Switches

- Cables:
  - Serial
  - Twisted pair
  - Coax

---

# Endpoint configuration

- Endpoint config is essential
  - On different Linux distributions, NIC configuration is different:
    - On **Debian Based**:
      - `/etc/network/interfaces`
      - On desktops version there usually will be `NetworkManager` Daemon to manage network for us.
      - Also can be managed with `net-tools` toolkit, but needs to be manually installed. (security issues)
      - `IP` toolkit is provided out of the box.
    - On **RedHat Based**:
      - `/etc/sysconfig/network-scripts/ifcfg-[interface]`
        - `[interface]`: each NIC has its own on configuration file.
      - `NetworkManager` Daemon to manage network for us out of the box.
      - Also can be managed with `net-tools` toolkit, but needs to be manually installed. (security issues)
      - `IP` toolkit is provided out of the box.

---
# Endpoint configuration

- Configuring **Debian Based** NIC:
  
```ini
root@debian11:~# cat /etc/network/interfaces
#This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).
# The loopback network interface
auto lo
iface lo inet loopback

auto enp0s3 # might be different --> 
            # depends on hardware naming -->
            # can be changed on kernel config.
iface eth0 inet dhcp # getting up via DHCP server
```
---

# Endpoint configuration

- Configuring **Debian Based** NIC:

```ini
auto lo
iface lo inet loopback

auto  enp0s3
iface enp0s3 inet static
address   10.100.189.198
broadcast 10.100.189.207
netmask   255.255.255.240
gateway   10.100.189.193
```
---
# Endpoint configuration

- Configuring **RedHat Based** NIC:
  
```sh
root@Rocky8:~# cat /etc/sysconfig/network-script/ifcfg-enp0s3
DEVICE="enp0s3"
HWADDR="00:22:44:66:AA:FF"
NM_CONTROLLED="no"
BOOTPROTO="dhcp"
ONBOOT="yes"
```
---

# Endpoint configuration

- Configuring **RedHat Based** NIC:

```sh
root@Rocky8:~# cat /etc/sysconfig/network-script/ifcfg-enp0s3
TYPE="Ethernet"
BOOTPROTO="dhcp"
DEFROUTE="yes"P
EERDNS="yes"
PEERROUTES="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="yes"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_PEERDNS="yes"
IPV6_PEERROUTES="yes"
IPV6_FAILURE_FATAL="no"
NAME="enp0s3"
UUID="9fa6a83a-2f8e-4ecc-962c-5f614605f4ee"
DEVICE="enp0s3"
ONBOOT="yes"
```

---

# Exercise

- Create clean debian based OS (VMware/Virtualbox/Vagrant/Docker/anyway you like)
- Configure 2 interfaces on that VM
- Log in to the machine:
  - Configure one interface to accept IP from DHCP server
  - Configure second interface to accept static configuration and to be routed via network of 10.100.102.0/24 with ip of 10.100.102.200

---

# Network Manager

Although we went through configuring static and dynamic configurations, one question that might rise would be: "So, if I am running on different distro, i'd need to have different config ?". Essentially, the would be "yes", although there is other way to configure Debian based and RedHat based systems network, with one tool named Network-Manager-Command-Line-Interface, or `nmcli` for short. There is also more guided version named Network-Manager-Terminal-User-Iterface or `nmtui`. So what can it do ?

- Nmcli/nmtui:
  - Utility for creating and managing network with `NetworkManager` daemon.
  - Single tool for all distributions.
  - Can manage various configuration.
  - Connections are stored in configuration files

> Note: The NetworkManager service MUST be running for us to use nmcli/nmtui

